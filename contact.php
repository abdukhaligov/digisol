<?php include("modules/header.php"); ?>
    <div class="content">
        <div class="contact_header_img"></div>
        <div class="contact_res">
            <div class="contact_text">
                <div class="row">
                    <div class="col-6">
                        <div class="contact_left_text"><p class="all">Lorem ipsum</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.</p></div>
                    </div>
                    <div class="col-6">
                        <div class="contact_right_text"><p class="all">Celephone </p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.</p></div>
                    </div>
                </div>
            </div>
            <div class="contact_us"><h3>Contact Us</h3>
                <p class="ff">For applying jobs. please send your CV to sales@digisol.ae</p>
                <div class="row">
                    <div class="col-6">
                        <div class="contact_us_text_mobile">
                            <div class="contact_map">
                                <div class="us_mobile_radius"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                <p>Suite No #704 Dar Al Riffa Building </p>
                                <p>Khalid Bin Waleed road Burdubai</p>
                                <p> - Dubai, UAE Land Mark :</p>
                                <p>Opposite Ascot Hotel , Bur Dubai</p></div>
                            <div class="contact_phone">
                                <div class="us_mobile_radius"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                <p>97143861414</p></div>
                            <div class="contact_faks">
                                <div class="us_mobile_radius"><i class="fa fa-fax"></i></div>
                                <p>97143861414</p></div>
                        </div>
                        <div class="contact_register_web"><input type="text" name="" placeholder="Name" class="us">
                            <input type="text" name="" placeholder="E-mail Adress" class="us"> <input type="text"
                                                                                                      name=""
                                                                                                      placeholder="Phone Number"
                                                                                                      class="us"> <input
                                    type="text" name="" placeholder="Enter The Product Name" class="us"> <input
                                    type="text" name="" placeholder="Message" class="us"> <input type="submit" name=""
                                                                                                 value="SEND MESSAGE"
                                                                                                 class="contact_submit">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="contact_us_text_web">
                            <div class="contact_icon"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                <p>Suite No #704 Dar Al Riffa Building </p>
                                <p>Khalid Bin Waleed road Burdubai</p>
                                <p> - Dubai, UAE Land Mark :</p>
                                <p>Opposite Ascot Hotel , Bur Dubai</p></div>
                            <div class="contact_icon"><i class="fa fa-phone" aria-hidden="true"></i>
                                <p>97143861414</p></div>
                            <div class="contact_icon"><i class="fa fa-fax"></i>
                                <p>97143861414</p></div>
                        </div>
                        <div class="contact_register_mobile"><input type="text" name="" placeholder="Name" class="us">
                            <input type="text" name="" placeholder="E-mail Adress" class="us"> <input type="text"
                                                                                                      name=""
                                                                                                      placeholder="Phone Number"
                                                                                                      class="us"> <input
                                    type="text" name="" placeholder="Enter The Product Name" class="us"> <input
                                    type="text" name="" placeholder="Message" class="us"> <input type="submit" name=""
                                                                                                 value="SEND MESSAGE"
                                                                                                 class="contact_submit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d925128.4105226778!2d55.227747!3d25.075348!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0x74ced0bf2b0029e9!2z0JTRg9Cx0LDQuSAtINCe0LHRitC10LTQuNC90LXQvdC90YvQtSDQkNGA0LDQsdGB0LrQuNC1INCt0LzQuNGA0LDRgtGL!5e0!3m2!1sru!2sus!4v1519301867648"
                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div><?php include("modules/footer.php") ?>