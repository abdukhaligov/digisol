<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- ========= RESPONSIVES By AGHA ======== !-->
    <link rel="stylesheet" type="text/css" href="css/responsiv.css">
    <!-- ========= RESPONSIVES By AGHA ======== !-->
    <link rel="stylesheet" type="text/css" href="css/hamburger.css">
    <link rel="stylesheet" type="text/css" href="css/swiper.css">
    <link rel="stylesheet" href="css/4mobile.css">
    <link rel="stylesheet" type="text/css" href="css/totop.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/my.js"></script>
    <script src='js/device.min.js'></script>
</head>
<!--   <body id="style-1"> -->
<body>
<!-- <div class="force-overflow"></div> -->
<div id="navbar" class="head_1">
    <div class="brand">
        <a href="./" style="float: left;">
            <div class="logo_bg">
                <img src="css/img/logo.png">
            </div>
        </a>
        <div style="cursor: pointer;" class="section">
            <img class="menu_img on_img active_img" src="css/img/hamb_close.png">
            <img class="menu_img off_img close_img" src="css/img/hamb_active.png">
            <div class="menu menu_nav">MENU</div>
        </div>
    </div>
</div>
<header id="main-nav">
    <div class="head_1">
        <div class="brand">
            <a href="./" style="float: left;">
                <div class="logo_bg">
                    <img src="css/img/logo.png">
                </div>
            </a>
            <div class="section_main">
                <div style="cursor: pointer;" class="section">
                    <img class="menu_img on_img active_img" src="css/img/hamb_close.png">
                    <img class="menu_img off_img close_img" src="css/img/hamb_active.png">
                    <div class="menu">MENU</div>
                </div>
                <!-- ========= HAMBURGER BEGIN ======= !-->
                <div class="hamb">
                    <div class="hamb_current"><img src="./css/img/nav_home.png">Home</div>
                    <div class="hamb_nav hamb_nav_pr"><img src="./css/img/nav_products.png">Products
                    </div>
                    <div class="hamb_submenu" id="style-1">
                        <div class="force-overflow"></div>
                        <div class="hamb_submenu_in">
                            <a href="./product.php">
                                <div class="hamb_products">POS SYSTEMS</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">BARCODE PRINTER</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">BARCODE SCANNER</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">POS PERIPHERALS</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">RIBBONS & CONSUMABLES</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">WEIGHING SYSTEMS</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">ANTI THEFT SYSTEMS</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">SPECIAL PRODUCTS</div>
                            </a>
                            <a href="./product.php">
                                <div class="hamb_products">DRIVERS</div>
                            </a>
                        </div>
                    </div>
                    <div class="hamb_nav hamb_nav_pr_2"><img src="./css/img/nav_sol.png">Solution
                    </div>
                    <div class="hamb_submenu_2" id="style-1">
                        <div class="force-overflow"></div>
                        <div class="hamb_submenu_in_2">
                            <a href="./solution.php">
                                <div class="hamb_products">POS SYSTEMS</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">BARCODE PRINTER</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">BARCODE SCANNER</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">POS PERIPHERALS</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">RIBBONS & CONSUMABLES</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">WEIGHING SYSTEMS</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">ANTI THEFT SYSTEMS</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">SPECIAL PRODUCTS</div>
                            </a>
                            <a href="./solution.php">
                                <div class="hamb_products">DRIVERS</div>
                            </a>
                        </div>
                    </div>
                    <a href="./about.php">
                        <div class="hamb_nav"><img src="./css/img/nav_about.png">About Us</div>
                    </a>
                    <a href="./support.php">
                        <div class="hamb_nav"><img src="./css/img/nav_support.png">Support</div>
                    </a>
                    <a href="./contact.php">
                        <div class="hamb_nav"><img src="./css/img/nav_contact.png">Contact Us</div>
                    </a>
                    <!--                    <div class="hamb_sub">-->
                    <!--                        <div class="hamb_data">-->
                    <!--                            <span id="txt"> </span>-->
                    <!--                            --><?php //echo date('\, F Y'); ?>
                    <!--                            <div style="margin-top: 15px;">-->
                    <!--                                --><?php //echo date('l jS '); ?>
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div>
                <!-- ========= HAMBURGER END ======= !-->
            </div>
        </div>
    </div>
    <div class="head_2" id="head_2">
        <div class="brand brand_2">
            <div class="logo_sm">
                <img src="css/img/logo_1.png" style="float: left;">
            </div>
            <div class="head_contact">
                <div style="float: left;"><img src="css/img/headphn.png"></div>
                <span>00971 41 00 1010</span>
            </div>
            <div class="center_icons">
                <div class="header_icon1">
                    <i class="fa fa-facebook-f"></i>
                </div>
                <div class="header_icon2">
                    <i class="fa fa-twitter"></i>
                </div>
                <div class="header_icon3">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </div>
                <div class="header_icon4">
                    <i class="fa fa-instagram"></i>
                </div>
            </div>
            <div class="language">
                <select>
                    <option>az</option>
                    <option>en</option>
                    <option>ru</option>
                </select>
            </div>
            <div class="center_search">
                <input type="text" name="" placeholder="search...">
                <a href="#">
                    <div class="searc_i"><img src="./css/img/search.png"></div>
                </a>
            </div>
        </div>
    </div>
</header>