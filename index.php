<?php include ("modules/header.php");?>

<!-- <div class="video-banner">
  <div class="videoWrapper">
    <iframe width="640" height="480" src="//www.youtube.com/embed/EJqo90lNYLs?rel=0&autoplay=1&showinfo=0&controls=0&HD=1" frameborder="0" allowfullscreen></iframe>
</div>
</div> -->

<div class="video-banner" style="display: table;">
<div class="video-background">
    <div class="video-foreground">
      <iframe src="https://www.youtube.com/embed/X-dMOvEOQiM?controls=0&amp;mute=1&amp;showinfo=0&amp;rel=0&amp;autoplay=1&amp;loop=1&amp;playlist=X-dMOvEOQiM" frameborder="0" allowfullscreen=""></iframe>
    </div>
  </div>
  <div class="video-title">
  <span class="video-h">COMPLETE BARCODE SYSTEM</span>
  <P>LOREM IPSUM LOREM IPSUM LOREM IPSUM</P>
  <input class="video-button" type="submit" name="" value="READ MORE">
</div>
  </div>  
 <div class="home_img_m"></div>
  <!-- =============================================== -->
          
  <!-- =============================================== -->
<div class="content" style="background-color: white;">
  <div class="services">
      <h2>SOLUTION</h2>
      <div class="services_radius">
        <div class="services_icon">
          <img src="css/img/sol_hypermarket.png">
        </div>
        <div class="services_text">
          <h3>Hypermarket</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut </p>
            <input type="submit" name="" value="READ MORE">
        </div>
      </div>
      <div class="services_radius">
          <div class="services_icon">
            <img src="css/img/sol_fashion.png">
          </div>
        <div class="services_text">
          <h3>Fashion outlets</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut </p>
            <input type="submit" name="" value="READ MORE">
        </div>
      </div>
      <div class="services_radius">
          <div class="services_icon">
            <img src="css/img/sol_restaurant.png">
          </div>
        <div class="services_text">
          <h3>Restaurant</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut </p>
            <input type="submit" name="" value="READ MORE">
          
        </div>
      </div>
      <div class="services_radius">
          <div class="services_icon">
            <img src="css/img/sol_hospital.png">
          </div>
        <div class="services_text">
          <h3>Hospital</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut </p>
            <input type="submit" name="" value="READ MORE">
        </div>
      </div>
      <div class="services_radius">
          <div class="services_icon">
            <img src="css/img/sol_warehouse.png">
          </div>
        <div class="services_text">
          <h3>Warehouse</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut </p>
            <input type="submit" name="" value="READ MORE">
        </div>
      </div>
    </div>
<!-- ================================================= -->
        
<!-- ================================================= -->
<div class="home_res">
  <div class="gallery">
      <div class="row">
        <div class="col-5">
          <div class="gallery_text_web">
              <p class="all">Restaurant Automation Admission System</p>
              <p>Restoran Avtomatlaşdırma Proqramı yiyəcək içəcək sektorunda xidmət göstərən restoran,bar,kafe kimi müəssisələrdə satış və ticarət əməliyyatlarıını icra etmək üçün bir proqramdır.</p>
          </div>
        </div>
        <div class="col-7">
          <div class="gallery_img">
            <img src="css/img/ind_1.jpg">
            <div class="gallery_text_mobile mob_wid">
                <p class="all">Restaurant Automation Admission System</p>
                <p>Restoran Avtomatlaşdırma Proqramı yiyəcək içəcək sektorunda xidmət göstərən restoran,bar,kafe kimi müəssisələrdə satış və ticarət əməliyyatlarıını icra etmək üçün bir proqramdır.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-7">
          <div class="gallery_img">
          <img src="css/img/ind_2.jpg">
          </div>
        <div class="gallery_text_mobile mob_widh">
            <p class="all">Hyppermarket Automation Admission System</p>
            <p>Suppermarket avtomatlaşdırma proqramı ilə günün yükünü azaltmaq və   müştərilərinizə tez bir zamanda xidmət göstərə bilərsiniz.
                Onlarla tez və rahat hesabat apara bilərsiniz və asanlıqla faktura-çek-istiqraz-çatdırılma-bank modullarından istifadə edə bilərsiniz.</p>
        </div>
        </div>
        <div class="col-5">
        <div class="gallery_text_web">
            <p class="all">Hyppermarket Automation Admission System</p>
            <p>Suppermarket avtomatlaşdırma proqramı ilə günün yükünü azaltmaq və   müştərilərinizə tez bir zamanda xidmət göstərə bilərsiniz.
                Onlarla tez və rahat hesabat apara bilərsiniz və asanlıqla faktura-çek-istiqraz-çatdırılma-bank modullarından istifadə edə bilərsiniz.</p>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-5">
          <div class="gallery_img">
            <img src="css/img/ind_3.jpg">
          <div class="box_mobile">
              <p class="all">FASHION OUTLET Automation Admission System</p>
              <p>Satış yaparken hızlı satış, peşin satış, taksitli satış gibi seçeneklerle rahatlıkla satış işlemlerinizi gerçekleştirebilirsiniz. İşinizin akışına göre işlerinize kolaylık sağlayacak olan paket programlar, zamanı gelen taksitlerinizi de anında görebilirsiniz.</p>
          </div>
          </div>
        </div>
        <div class="col-7">
          <div class="gallery_img">
            <img src="css/img/ind_4.jpg">
          <div class="gallery_text_mobile mob_wid">
              <p class="all">Hospital Automation Admission System</p>
              <p>İstifadəyə təqdim edilən dərmanların üzərindəki barkodu oxudaraq sistemə qeydə olub olmadığını sorğulayıp dərman haqqında ətraflı məlumatlar ala biləcəyiniz bir proqramdır.</p>
          </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-5">
          <div class="gallery_text_web">
              <p class="all">FASHION OUTLET Automation Admission System</p>
              <p>Satış yaparken hızlı satış, peşin satış, taksitli satış gibi seçeneklerle rahatlıkla satış işlemlerinizi gerçekleştirebilirsiniz. İşinizin akışına göre işlerinize kolaylık sağlayacak olan paket programlar, zamanı gelen taksitlerinizi de anında görebilirsiniz.</p>
          </div>
        </div>
        <div class="col-7">
          <div class="box_web">
              <p class="all">Hospital Automation Admission System</p>
              <p>İstifadəyə təqdim edilən dərmanların üzərindəki barkodu oxudaraq sistemə qeydə olub olmadığını sorğulayıp dərman haqqında ətraflı məlumatlar ala biləcəyiniz bir proqramdır.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-7">
          <div class="gallery_img">
            <img src="css/img/ind_5.jpg">
            <div class="gallery_text_mobile mob_wid">
                <p class="all">Travel Automation Admission System</p>
                <p class="ll">Satış yaparken hızlı satış, peşin satış, taksitli satış gibi seçeneklerle rahatlıkla satış işlemlerinizi gerçekleştirebilirsiniz. İşinizin akışına göre işlerinize kolaylık sağlayacak olan paket programlar, zamanı gelen taksitlerinizi de anında görebilirsiniz.</p>
          </div>
          </div>
        </div>
        <div class="col-5">
          <div class="gallery_img">
            <img src="css/img/ind_6.jpg">
          <div class="box_mobile_">
              <p class="all">Celephone Shop Automation Admission Syste</p>
              <p>Satış yaparken hızlı satış, peşin satış, taksitli satış gibi seçeneklerle rahatlıkla satış işlemlerinizi gerçekleştirebilirsiniz. İşinizin </p>
          </div>
          </div>
          
        </div>
      </div>
      <div class="row">
        <div class="col-7">
          <div class="gallery_text_web">
              <p class="all">Travel Automation Admission System</p>
              <p class="ll">Satış yaparken hızlı satış, peşin satış, taksitli satış gibi seçeneklerle rahatlıkla satış işlemlerinizi gerçekleştirebilirsiniz. İşinizin akışına göre işlerinize kolaylık sağlayacak olan paket programlar, zamanı gelen taksitlerinizi de anında görebilirsiniz.</p>
          </div>
        </div>
        <div class="col-5">
          <div class="box_web_">
              <p class="all">Celephone Shop Automation Admission Syste</p>
              <p>Satış yaparken hızlı satış, peşin satış, taksitli satış gibi seçeneklerle rahatlıkla satış işlemlerinizi gerçekleştirebilirsiniz. İşinizin </p>
          </div>
        </div>
      </div>
    </div>
<!--==================================================  -->
      
<!-- ================================================= -->
    <div class="pacgakes">
      <div class="row">
        <div class="col-4">
          <div class="pacgakes_block">
            <h3>lorem ipsum</h3>
            <p class="xh">Lorem ipsum </p>
            <p class="xh">Dolor sit amet, consect</p>
            <p class="xh">Adipiscing elit</p>
            <p class="xh">Proin commodo turpis</p>
            <p class="xh">lacus pulvinarvel </p>
            <p class="xh">Prnare nisi pretium.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="pacgakes_block">
            <h3>lorem ipsum</h3>
            <p class="xh">Lorem ipsum </p>
            <p class="xh">Dolor sit amet, consect</p>
            <p class="xh">Adipiscing elit</p>
            <p class="xh">Proin commodo turpis</p>
            <p class="xh">lacus pulvinarvel </p>
            <p class="xh">Prnare nisi pretium.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="pacgakes_block">
            <h3>lorem ipsum</h3>
            <p class="xh">Lorem ipsum </p>
            <p class="xh">Dolor sit amet, consect</p>
            <p class="xh">Adipiscing elit</p>
            <p class="xh">Proin commodo turpis</p>
            <p class="xh">lacus pulvinarvel </p>
            <p class="xh">Prnare nisi pretium.</p>
          </div>
        </div>
      </div>
    </div>
    </div>
<!-- ================================================= -->
    
<!-- ================================================ -->
      
  <div class="text" style="-webkit-clip-path: polygon(0 0, 100% 24%, 100% 100%, 0% 100%);
clip-path: polygon(0 0, 100% 24%, 100% 100%, 0% 100%);">
<div style="max-width: 1200px; margin: auto;">
      <h1>Lorem ipsum</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillumdolore eu fugiat nulla pariatur.</p>
      
      <a href="#">Read More</a>
  </div>
    </div>
<!-- ================================================ -->

<div class="home_res">
  <div class="testimonial">
      <h1>TESTIMONIAL</h1>
  <div class="testimonial_mob swiper-container sw_tes_1">
    <div class="swiper-wrapper h_mrg-swppagn">
      <div class="swiper-slide">
              <div class="row">
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_1.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Alexis Robinson</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
      </div>
      </div>
            <div class="swiper-slide">
              <div class="row">
               <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_2.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Edwards Robinson</h3>
          <p class="Edwards">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
          </div>
        </div>
      </div>
      </div>
            <div class="swiper-slide">
              <div class="row">
         <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_3.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Kate Brown</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
      </div>
      </div>
            
                  <div class="swiper-slide">
              <div class="row">
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_1.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Alexis Robinson</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
      </div>
      </div>
            <div class="swiper-slide">
              <div class="row">
               <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_2.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Edwards Robinson</h3>
          <p class="Edwards">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
          </div>
        </div>
      </div>
      </div>
            <div class="swiper-slide">
              <div class="row">
         <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_3.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Kate Brown</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
      </div>
      </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination swp_tes_1"></div>
  </div>
              <!-- Swiper -->
  <div class="testimonial_web swiper-container sw_tes_1"  >
    <div class="swiper-wrapper h_mrg-swppagn">
      <div class="swiper-slide">
              <div class="row">
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_1.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Alexis Robinson</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_2.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Edwards Robinson</h3>
          <p class="Edwards">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
          </div>
        </div>
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_3.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Kate Brown</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
      </div>
      </div>
      <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_1.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Alexis Robinson</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_2.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Edwards Robinson</h3>
          <p class="Edwards">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
          </div>
        </div>
        <div class="col-4">
          <div class="testimonial_radius">
            <img src="css/img/testim_3.jpg">
          </div>
          <div class="testimonial_text">
            <h3>Kate Brown</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been </p>
          </div>
        </div>
      </div>
      </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination swp_tes_1"></div>
  </div>
     
    </div>
<!-- ================================================ -->
  <style type="text/css">
    .testimonial_mob .swiper-pagination-bullet{
        width: 20px;
        height: 20px;
    }
    .testimonial_web{
      height: min-content;

    }
    .h_mrg-swppagn{
      margin-bottom: 60px;
    }
  </style>

  <div class="testimonial">
      <h1>BRANDS</h1>
<div class="swiper-container testimonial_mob sw_tes_2">
    <div class="swiper-wrapper h_mrg-swppagn">
      <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/cff9028b8d3bd2b38df4bd8aa18f82ba (1).png">
          </div>
        </div>
      </div>
    </div>
          <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/af287c51fd1c98e1e0115a1d17b37d4b.png">
          </div>
        </div>
      </div>
    </div>
          <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/6b6d12f95e370a5847d62799e9b0c3ce.png">
          </div>
        </div>
      </div>
      </div>
            <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/cff9028b8d3bd2b38df4bd8aa18f82ba (1).png">
          </div>
        </div>
      </div>
    </div>
          <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/af287c51fd1c98e1e0115a1d17b37d4b.png">
          </div>
        </div>
      </div>
    </div>
          <div class="swiper-slide">
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/6b6d12f95e370a5847d62799e9b0c3ce.png">
          </div>
        </div>
      </div>
      </div>
    </div>
          <!-- Add Pagination -->
    <div class="swiper-pagination_1 swp_tes_2"></div>
  </div> 
 <div class="swiper-container_1 testimonial_web sw_tes_2">
    <div class="swiper-wrapper h_mrg-swppagn">
      <div class="swiper-slide">
        
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/cff9028b8d3bd2b38df4bd8aa18f82ba (1).png">
          </div>
        </div>
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/af287c51fd1c98e1e0115a1d17b37d4b.png">
          </div>
        </div>
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/6b6d12f95e370a5847d62799e9b0c3ce.png">
          </div>
        </div>
      </div>
      </div>
            <div class="swiper-slide">
              
      <div class="row">
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/cff9028b8d3bd2b38df4bd8aa18f82ba (1).png">
          </div>
        </div>
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/af287c51fd1c98e1e0115a1d17b37d4b.png">
          </div>
        </div>
        <div class="col-4">
          <div class="brands_img">
            <img src="css/img/6b6d12f95e370a5847d62799e9b0c3ce.png">
          </div>
        </div>
      </div>
      </div>
    </div>
          <!-- Add Pagination -->
    <div class="swiper-pagination_1 swp_tes_2"></div>
  </div> 
    </div>
    </div>
<!-- ================================================ -->
  <div class="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d925128.4105226778!2d55.227747!3d25.075348!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0x74ced0bf2b0029e9!2z0JTRg9Cx0LDQuSAtINCe0LHRitC10LTQuNC90LXQvdC90YvQtSDQkNGA0LDQsdGB0LrQuNC1INCt0LzQuNGA0LDRgtGL!5e0!3m2!1sru!2sus!4v1519301867648" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      <div class="contact">
        <h3>Contact Us</h3>
        <p>For applying jobs. please send</p>
        <p>your CV to sales@digisol.ae</p>
        <input type="text" name="" placeholder="Name" class="us">
        <input type="text" name="" placeholder="E-mail Adress" class="us">
        <input type="text" name="" placeholder="Phone Number" class="us">
        <input type="text" name="" placeholder="Enter The Product Name" class="us">
        <input type="text" name="" placeholder="Message" class="us">
        <input type="submit" name="" value="SEND MESSAGE" class="contact_submit">
      </div>
    </div>
</div>

<?php include ("modules/footer.php") ?>
