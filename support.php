<?php include ("modules/header.php");?>
<div class="content">
   <div class="support_img"></div>
   <div class="support_res">
      <div class="support">
         <h3>SUPPORT</h3>
         <div class="support_icon_up">
            <div class="row">
               <div class="col-6">
                  <div class="support_icon">
                     <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </div>
                  <div class="support_radius_text">
                     <p class="all">ÜNVAN</p>
                     <p>Suite No #704 Dar Al Riffa Building Khalid Bin Waleed road Burdubai 
                        - Dubai, UAE Land Mark : 
                        Opposite Ascot Hotel , Bur Dubai
                     </p>
                  </div>
               </div>
               <div class="col-6">
                  <div class="support_icon">
                     <i class="fa fa-fax"></i>
                  </div>
                  <div class="support_radius_text">
                     <p class="all">FAX</p>
                     <p>97143861414</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="support_icon_low">
            <div class="row">
               <div class="col-6">
                  <div class="support_icon">
                     <i class="fa fa-phone" aria-hidden="true"></i>
                  </div>
                  <div class="support_radius_text">
                     <p class="all">TELEFON</p>
                     <p>971527559405</p>
                  </div>
               </div>
               <div class="col-6">
                  <div class="support_icon">
                     <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                  <div class="support_radius_text">
                     <p class="all">E-mail adress</p>
                     <p>For applying jobs. please send your CV to sales@digisol.ae</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="support_low">
         <div class="row">
            <div class="col-6">
               <div class="support_low_icons">
                  <div class="low_icon color1">
                     <i class="fa fa-facebook-f"></i>
                  </div>
                  <div class="low_icon color2">
                     <i class="fa fa-twitter"></i>
                  </div>
                  <div class="low_icon color3">
                     <i class="fa fa-linkedin" aria-hidden="true"></i>
                  </div>
                  <div class="low_icon color4">
                     <i class="fa fa-instagram"></i>
                  </div>
                  <br>
                  <br>
                  <p>© 2016 | DIGISOL. All rights reserved.</p>
                  <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
               </div>
            </div>
            <div class="col-6">
               <div class="support_register">
                  <input type="text" name="" placeholder="User name" class="support_input">
                  <br>
                  <input type="text" name="" placeholder="E-mail -adress" class="support_input">
                  <br>
                  <input type="text" name="" placeholder="subject" class="support_input">
                  <br>
                  <textarea></textarea>
                  <br>
                  <input type="submit" name="" value="Send message" class="support_submit">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clear"></div>
<?php include ("modules/footer.php") ?>