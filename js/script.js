function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}

/* cookie.JS
 ========================================================*/
include('js/jquery.cookie.js');
/* Easing library
 ========================================================*/
include('js/jquery.easing.1.3.js');
/* Stick up menus
 ========================================================*/
;
/* ToTop
 ========================================================*/
;
(function($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        include('js/jquery.ui.totop.js');

        $(document).ready(function() {
            $().UItoTop({
                easingType: 'easeOutQuart',
                containerClass: 'toTop fa-angle-up'
            });
        });
    }
})(jQuery);


var test = $("#test").html();
