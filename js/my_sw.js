    var swiper_testimonial = new Swiper('.sw_tes_1', {

      pagination: {

        el: '.swp_tes_1',

        clickable: true,

      },
      speed: 750,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },

      slidesPerView: 1,

      spaceBetween: 30,

      loop: true,



      navigation: {

        nextEl: '.swiper-button-next',

        prevEl: '.swiper-button-prev',

      },



    });



    var swiper_solution = new Swiper('.sw_sol', {

      pagination: {

        el: '.swp_sol',

        clickable: true,

      },
      speed: 750,
      autoplay: {
        delay: 4000,
        disableOnInteraction: false,
      },


      slidesPerView: 1,

      spaceBetween: 30,

      loop: true,



      navigation: {

        nextEl: '.swiper-button-next',

        prevEl: '.swiper-button-prev',

      },



    });





    var swiper_brands = new Swiper('.sw_tes_2', {

      pagination: {

        el: '.swp_tes_2',

        clickable: true,

      },
      speed: 750,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },

      slidesPerView: 1,

      spaceBetween: 30,

      loop: true,

    });



    var swiper_3 = new Swiper('.sw_prod', {

      pagination: {

        el: '.swp_prod',

        type: 'fraction',

      },

      loop: true,

      navigation: {

        nextEl: '.sw_prod_bn',

        prevEl: '.sw_prod_bp',

      },

    });